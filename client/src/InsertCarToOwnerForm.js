import React from 'react';

class InsertCarToOwnerForm extends React.Component {
    constructor(props) {
      super(props);
      this.state = {key: ''};
      this.state = {value: ''};
  
      this.handleKeyChange = this.handleKeyChange.bind(this);
      this.handleValueChange = this.handleValueChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    handleKeyChange(event) {
      this.setState({key: event.target.value});
    }
    handleValueChange(event) {
      this.setState({value: event.target.value});
    }
    handleSubmit(event) {
      alert('A car to owner was submitted: ' + this.state.value +". Trying to Send to BE :)");

      const requestOptions = {
            mode: 'cors',    
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' 
                },
            
        };

        fetch(`https://localhost:5001/TestingConroller?key=${this.state.key}&value=${this.state.value}`, requestOptions);

        event.preventDefault();
    }
  
    render() {
      return (
        <div>
          <form onSubmit={this.handleSubmit}>
            please enter car to owner to save:
            Key: <input type="text" value={this.state.Key} onChange={this.handleKeyChange} name="model"></input>
            Value: <input type="text" value={this.state.value} onChange={this.handleValueChange} name="model"></input>
            <input type="submit" value="Submit" />
        </form>
        <p>{this.state.value2}</p>
        </div>
      );
    }
  }
  export default InsertCarToOwnerForm;