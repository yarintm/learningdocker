import React from 'react';
import InsertModelForm from './InsertModelForm';
import './App.css';
import InsertCarToOwnerForm from './InsertCarToOwnerForm';
import GetModelForm from './GetModelForm';

function App() {
  return (
    <div className="App">
      <header className="App-header">
      <InsertModelForm/>
      <InsertCarToOwnerForm/>
      <GetModelForm/>
      </header>
    </div>
  );
}

export default App;
