import React from 'react';

class InsertModelForm extends React.Component {
    constructor(props) {
      super(props);
      this.state = {value: ''};
      this.state = {value2: 'hello world!'};
  
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    handleChange(event) {
      this.setState({value: event.target.value});
    }
  
    handleSubmit(event) {
      alert('A name was submitted: ' + this.state.value +". Trying to Send to BE :)");

      const requestOptions = {
            mode: 'cors',    
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' 
                },
            body: JSON.stringify({ model: this.state.value })
        };

        fetch('https://localhost:5001/TestingConroller', requestOptions);

        event.preventDefault();
    }
  
    render() {
      return (
        <div>
          <form onSubmit={this.handleSubmit}>
            please enter car model to save:
            <input type="text" value={this.state.value} onChange={this.handleChange} name="model"></input>
            <input type="submit" value="Submit" />
        </form>
        <p>{this.state.value2}</p>
        </div>
      );
    }
  }
  export default InsertModelForm;