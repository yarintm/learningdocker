import React from 'react';

class GetModelForm extends React.Component {
    constructor(props) {
      super(props);
      this.state = {key: ''};
      this.state = {value: ''};
  
      this.handleKeyChange = this.handleKeyChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    handleKeyChange(event) {
      this.setState({key: event.target.value});
    }
    handleSubmit(event) {
      const requestOptions = {
            mode: 'cors',    
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' 
                },
            
        };
        
        fetch(`https://localhost:5001/TestingConroller/OwnerOfCar/${this.state.key}`, requestOptions)
        .then(response => response.json())
        .then(data => this.setState({ value: data }));

        event.preventDefault();
    }
  
    render() {
      return (
        <div>
          <form onSubmit={this.handleSubmit}>
            please enter owner to get car model:
            owner: <input type="text" value={this.state.Key} onChange={this.handleKeyChange} name="model"></input>
            <input type="submit" value="Submit" />
        </form>
        <p>His car is : {this.state.value}</p>
        </div>
      );
    }
  }
  export default GetModelForm;