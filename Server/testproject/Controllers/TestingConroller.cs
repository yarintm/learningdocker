﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using StackExchange.Redis;

namespace testproject.Controllers
{
    
    [ApiController]
    [Route("[controller]")]
    public class TestingConroller : ControllerBase
    {
        [HttpGet("{name}")]
        public string Get(string name)
        {
            var output = "hello " + name;
            string path1 = "/output/file1";
            string path2 = "/Users/yarintamam/Desktop/output/file1";
            //Directory.Exists(path2);
            System.IO.File.AppendAllLines(path1, new[] {output});
            return output;
        }

        [HttpGet("OwnerOfCar/{key}")]
        public string GetFromRedis(string key)
        {
            ConnectionMultiplexer redis = ConnectionMultiplexer.Connect("localhost:6379");
            var db = redis.GetDatabase();
            var result = db.StringGet(key);
            return result;
        }

        [HttpPut()]
        public void GetFromRedis(string key, string value)
        {
            ConnectionMultiplexer redis = ConnectionMultiplexer.Connect("localhost:6379");
            var db = redis.GetDatabase();
            db.StringSet(key, value);

        }

        [HttpPost()]
        public void Post([FromBody] Model model)
        {
            string path1 = "/output/file1";
            string path2 = "/Users/yarintamam/Desktop/output/file1";
            //Directory.Exists(path2);
            System.IO.File.AppendAllLines(path2, new[] {model.model});
        }


    }
}
