## To run using docker:
docker rm /testprojectapp
docker build -t testprojectimage .
docker run -v ~/Desktop/output:/output -p 5000:80 --name testprojectapp testprojectimage

## To interact with go to: 
http://localhost:5000/swagger/index.html
